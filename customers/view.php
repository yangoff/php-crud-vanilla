<?php require_once('functions.php');
view($_GET['id']); ?>
<?php include(HEADER_TEMPLATE); ?>
    <h2>Cliente <?php echo $customer['id']; ?></h2>
    <hr>
<?php if (!empty($_SESSION['message'])) : ?>
    <div class="alert alert-<?php echo $_SESSION['type']; ?>"><?php echo $_SESSION['message']; ?></div>    <?php endif; ?>
    <dl class="row">
        <dt class="col-sm-3">Nome / Razão Social:</dt>
        <dd class="col-sm-9"><?php echo $customer['name']; ?></dd>
        <dt class="col-sm-3">CPF / CNPJ:</dt>
        <dd class="col-sm-9"><?php echo $customer['cpf_cnpj']; ?></dd>
        <dt class="col-sm-3">Data de Nascimento:</dt>
        <dd class="col-sm-9"><?php echo $customer['birthdate']; ?></dd>
    </dl>
    <dl class="row">
        <dt class="col-sm-3">Endereço:</dt>
        <dd class="col-sm-9"><?php echo $customer['address']; ?></dd>
        <dt class="col-sm-3">Bairro:</dt>
        <dd class="col-sm-9"><?php echo $customer['hood']; ?></dd>
        <dt class="col-sm-3">CEP:</dt>
        <dd class="col-sm-9"><?php echo $customer['zip_code']; ?></dd>
        <dt class="col-sm-3">Data de Cadastro:</dt>
        <dd class="col-sm-9"><?php echo $customer['created']; ?></dd>
    </dl>
    <dl class="row">
        <dt class="col-sm-3">Cidade:</dt>
        <dd class="col-sm-9"><?php echo $customer['city']; ?></dd>
        <dt class="col-sm-3">Telefone:</dt>
        <dd class="col-sm-9"><?php echo $customer['phone']; ?></dd>
        <dt class="col-sm-3">Celular:</dt>
        <dd class="col-sm-9"><?php echo $customer['mobile']; ?></dd>
        <dt class="col-sm-3">UF:</dt>
        <dd class="col-sm-9"><?php echo $customer['state']; ?></dd>
        <dt class="col-sm-3">Inscrição Estadual:</dt>
        <dd class="col-sm-9"><?php echo $customer['ie']; ?></dd>
    </dl>
    <div id="actions" class="row">
        <div class="col-md-12"><a href="edit.php?id=<?php echo $customer['id']; ?>" class="btn btn-primary">Editar</a>
            <a href="index.php" class="btn btn-default">Voltar</a></div>
    </div>
    <br>
<?php include(FOOTER_TEMPLATE); ?>