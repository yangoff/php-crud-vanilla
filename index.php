<?php require_once 'config.php'; ?>
<?php require_once DBAPI; ?>
<?php include(HEADER_TEMPLATE); ?>
<?php $db = open_database(); ?>

    <h1>Dashboard</h1>
    <hr/>

<?php if ($db) : ?>
    <div class="row">

        <div class="col-md-2">
            <a class='btn btn-primary btn-lg btn-block' href="customers/add.php" style='color:white;' >
                <div style='text-align:left;  color: white;'><i class="fa fa-plus fa-5x"></i></div>
                Novo Cliente
            </a>
        </div>
        <div class="col-md-2">
            <a class='btn  btn-lg btn-block' href="customers/index.php" style='background-color:transparent;' >
                <div style='text-align:left;'><i class="fa fa-user fa-5x icon-white"></i></div>
                Clientes
            </a>

        </div>
<?php else : ?>
    <div class="alert alert-danger" role="alert"><p><strong>ERRO:</strong> Não foi possível Conectar ao Banco de Dados!
        </p></div>
<?php endif; ?>
<?php include(FOOTER_TEMPLATE); ?>